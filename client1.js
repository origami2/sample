const assert = require('assert');
const ClientIO = require('origami2-socket.io').ClientIO;
const Key3 = require('./key3.json');

 new ClientIO(Key3.privateKey).connect('http://localhost:3000')
.then((api) => {
  api.SuperCalculator.average(100,20)
  .then(
    (average) => console.log(average),
    (err) => console.error(err)
  );
});

