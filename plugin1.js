const assert = require('assert');

function Calculator() {
}

Calculator.prototype.sum = function (a, b) {
  return a + b;
}

Calculator.prototype.divide = function (a, b) {
  return a / b;
}

const PluginIO = require('origami2-socket.io').PluginIO;
const Key2 = require('./key2.json');

assert(PluginIO);
assert(Key2.privateKey);

var plugin = new PluginIO(Calculator, Key2.privateKey);

plugin.connect('http://localhost:3000').then(() => {
console.log('connected');
}, (err) => {
console.error(err);
});
