const assert = require('assert');

function SuperCalculator(calculator) {
  this.calculator = calculator;
}

SuperCalculator.prototype.average = async function (value1, value2) {
  return await this.calculator.divide(await this.calculator.sum(value1, value2), 2);
};

const PluginIO = require('origami2-socket.io').PluginIO;
const Key4 = require('./key4.json');

assert(PluginIO);
assert(Key4.privateKey);

var plugin = new PluginIO(SuperCalculator, Key4.privateKey, {
  calculator: 'Calculator'
});

plugin.connect('http://localhost:3000').then(() => {
console.log('connected');
}, (err) => {
console.error(err);
});
