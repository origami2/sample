const assert = require('assert');

const Stack = require('origami2-core').Stack;
const StackIO = require('origami2-socket.io').StackIO;
const Key1 = require('./key1.json');
const Key2 = require('./key2.json');
const Key4 = require('./key4.json');
const http = require('http');

assert(Stack);
assert(StackIO);
assert(Key1);
assert(Key2);

var stack = new Stack(Key1.privateKey);

var stackio = new StackIO(stack);
stackio.authorizePlugin('Calculator', Key2.publicKey);
stackio.authorizePlugin('SuperCalculator', Key4.publicKey);

var server = http.createServer((req, res) => {
  res.write('hello');
  res.end();
});

assert(stack);
assert(stackio);
assert(server);

stackio.listenServer(server);

server.listen(3000);
